---
title: CI Experiments
subtitle: Experiments with GitLab CI/Pages
---

## Scope of this project

I want to deploy a bunch of .md files to GitLab Pages. This should not break other deployed files though, to make it minimally invasive.


table header 1 | table header 2 |
-------------- | -------------- |
content | content 2
content 3 | content 4

[Link](https://gitlab.com)

```java
public static void main(String[] args) {
    System.out.println("Hello, World!");
}
```

```sh
apt-get install zsh
```

This is some example with `inlined` text.


![Image example](https://www.tierheimhelden.de/magazin/wp-content/uploads/2020/01/zuscakova-basenka-_tMDkTl7zs8-unsplash-scaled.jpg)