import os


def convert_files():
    os.system('pandoc --standalone --template deploy/template.html README.md -o public/index.html')


if __name__ == "__main__":
    convert_files()
